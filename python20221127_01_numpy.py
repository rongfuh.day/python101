#使用套件 numpy
#在terminal內，安裝套件 
#用系統管理員，開啟Power Shell
# pip install numpy
# pip install matplotlib

#匯入套件，並簡稱為np
import numpy as np

#ndarray(n-dimensional array)是numpy存放資料的一種容器
#它很像list，但是它提供了很多複雜數學運算的功能
#網路參考資源 https://www.w3schools.com/python/numpy/numpy_creating_arrays.asp
#網路參考資源 https://numpy.org/learn/
#網路參考資源(圖示比較多) https://betterprogramming.pub/numpy-illustrated-the-visual-guide-to-numpy-3b1d4976de1d


#建立ndarray的方法
#作法一
arr1 = np.array([1, 2, 3])
arr2 = np.array([4, 5, 6])




print("第一次列印")
for i in arr1:
    print(i)

# 陣列每個元素都+1
arr1=arr1+1

print("第二次列印")
for i in arr1:
    print(i)


print( arr1.mean()) #求平均
print( arr1.max()) #找最大
print( arr1.min()) #找最小
print( arr1.var()) #變異數
print( arr1.std()) #標準差
print( arr1.ptp()) #全距


#a=arr1[arr1>2]



#作法二：產生一定範圍的陣列
arr1=np.arange(1,20)

#作法三：產生亂數陣列
arr1=np.random.randint(0,10,5)

print(arr1)

print(type(arr1))