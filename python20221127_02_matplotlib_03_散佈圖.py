#匯入套件，簡稱為np
import numpy as np
#匯入套件，簡稱為plt
import matplotlib.pyplot as plt

#美元匯率 https://rate.bot.com.tw/xrt/history?Lang=zh-TW
#黃金價格 https://rate.bot.com.tw/gold/passbook

#載入csv檔，分隔符號為","，跳過一列，用str格式解讀各欄位的資料，編碼方式為UTF-8
arr = np.loadtxt("file_usd_gold.csv", delimiter=",", skiprows=1, dtype=str, encoding="UTF-8")
#讀取二維陣列的編號1欄
usd=arr[:, 1]
gold=arr[:,2]

#將字串陣列轉為浮點數
usd=usd.astype(np.float)
gold=gold.astype(np.float)


#散佈圖
plt.scatter(usd,gold)
plt.show()

#相關係數
r=np.corrcoef(usd,gold)
print(np.corrcoef(usd,gold))

#簡單迴歸 黃金＝a*美元 + b
coef=np.polyfit(usd,gold,1) #得到迴歸的參數
print(coef)
reg_model=np.poly1d(coef) #用參數建立迴歸模型
print(reg_model) #印出迴歸公式

print(reg_model(30)) #用美金30元代入公式內

#將usd陣列代入reg_model
#產生了一個黃金價格陣列 
gold2=reg_model(usd) 

#散佈圖
plt.plot(usd,gold2, color="red") #用紅色線畫預測的金價與美元
plt.scatter(usd,gold, color="black") #用黑色點畫預測的金價與美元
plt.show()
