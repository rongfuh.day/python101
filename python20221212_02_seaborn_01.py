#使用套件 seaborn
#用系統管理員，開啟Power Shell
# pip install seaborn
#網路參考資源 https://seaborn.pydata.org/installing.html

#seaborn網路參考資源 https://seaborn.pydata.org/examples/index.html

#匯入套件，簡稱為np
import numpy as np

#匯入套件，簡稱為plt
import matplotlib.pyplot as plt

#匯入套件，簡稱為pd
import pandas as pd

import seaborn as sns
