#使用套件 pandas
#用系統管理員，開啟Power Shell
# pip install pandas

# pandas 網路參考資源：https://www.w3schools.com/python/pandas/default.asp

#匯入套件，簡稱為np
import numpy as np
#匯入套件，簡稱為plt
import matplotlib.pyplot as plt
#匯入套件，簡稱為pd
import pandas as pd



#載入csv檔，用dataframe格式存放資料


df = pd.read_csv('file_road_null.csv')
print(df)

#取得前三列資料
print(df.head(3))

#取得後三列資料
print(df.tail(3))


#用欄位名稱取得單一欄位的整欄資料
print( df["road"])

#at[資料索引值,欄位名稱] 利用資料索引值及欄位名稱來取得「單一值」
print( df.at[3,"road"])

#iat[資料索引值,欄位順序] 利用資料索引值及欄位順序來取得「單一值」
print( df.iat[3,1])

df2 = (df['city'] == '南投縣') & (df['site_id'] == '南投縣埔里鎮')
#取得前三列資料
print(df2.head(3))

#loc[資料索引值,欄位名稱] 利用資料索引值及欄位名稱來取得「資料集」
print( df.loc[[18,19],["exchange","gold"]])
#iloc[資料索引值,欄位順序]：利用資料索引值及欄位順序來取得「資料集」
print(df.iloc[[18, 19], [1, 2]])

#append()：新增一列的資料
df = df.append({
    "date": "20221129",
    "exchange": 60,
    "gold": 1690
}, ignore_index=True)
print( df.loc[[18,19,20],["date","exchange","gold"]])

#修改特定格子的值
df.at[20, "exchange"] = 33  #修改索引值為1的math欄位資料
df.iat[20, 2] = 1700
print( df.loc[[18,19,20],["date","exchange","gold"]])

#針對單一欄位排序
df = df.sort_values(["exchange"], ascending=False)
print(df["exchange"])

#篩選

print(df[ df["exchange"]>32])