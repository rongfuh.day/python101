#匯入套件，簡稱為np
import numpy as np
#匯入套件，簡稱為plt
import matplotlib.pyplot as plt

#陣列的切割 https://www.w3schools.com/python/numpy/numpy_array_slicing.asp
arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11,12,13,14,15]])
#取得2維陣列
print(arr[2, 3])
print(arr[:, 1])

#匯率參考資源 https://rate.bot.com.tw/xrt/quote/2022-11/USD
#台積電股價 https://finance.yahoo.com/quote/2330.TW/history?p=2330.TW

#載入csv檔，分隔符號為","，跳過一列，用str格式解讀各欄位的資料，編碼方式為UTF-8
#arr = np.loadtxt("file_exchange.csv", delimiter=",", skiprows=1, dtype=str, encoding="UTF-8")
arr = np.loadtxt("file_2301.csv", delimiter=",", skiprows=1, dtype=str, encoding="UTF-8")
#讀取二維陣列的編號1欄
arr2=arr[:, 1]

#將字串陣列轉為浮點數
arr3=arr2.astype(np.float)
#arr3 = np.array([10,4,6,14])

#趨勢線
plt.plot(arr3)
plt.show()

#直方圖
plt.hist(arr3,bins=10)
plt.show()

#箱型圖
plt.boxplot(arr3)
plt.show()

print( arr3.mean()) #求平均
print( arr3.max()) #找最大
print( arr3.min()) #找最小
print( arr3.var()) #變異數
print( arr3.std()) #標準差
print( arr3.ptp()) #全距


