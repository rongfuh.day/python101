#使用套件 pandas
#用系統管理員，開啟Power Shell
# pip install pandas

# pandas 網路參考資源：https://www.w3schools.com/python/pandas/default.asp

#匯入套件，簡稱為np
import numpy as np
#匯入套件，簡稱為plt
import matplotlib.pyplot as plt
#匯入套件，簡稱為pd
import pandas as pd


#美元匯率 https://rate.bot.com.tw/xrt/history?Lang=zh-TW
#黃金價格 https://rate.bot.com.tw/gold/passbook

# dataframe 網路參考資源 https://www.w3schools.com/python/pandas/pandas_dataframes.asp
# dataframe 網路參考資源 https://www.learncodewithmike.com/2020/11/python-pandas-dataframe-tutorial.html

#載入csv檔，用dataframe格式存放資料
df = pd.read_csv("file_usd_gold.csv")
print(df)

#各欄位的描述統計
print(df.describe())

#取得前三列資料
print(df.head(3))

#取得後三列資料
print(df.tail(3))

#用欄位名稱取得單一欄位的整欄資料
print( df["exchange"])

#at[資料索引值,欄位名稱] 利用資料索引值及欄位名稱來取得「單一值」
print( df.at[19,"exchange"])

#iat[資料索引值,欄位順序] 利用資料索引值及欄位順序來取得「單一值」
print( df.iat[19,1])

#loc[資料索引值,欄位名稱] 利用資料索引值及欄位名稱來取得「資料集」
print( df.loc[[18,19],["exchange","gold"]])
#iloc[資料索引值,欄位順序]：利用資料索引值及欄位順序來取得「資料集」
print(df.iloc[[18, 19], [1, 2]])

#append()：新增一列的資料
df = df.append({
    "date": "20221129",
    "exchange": 60,
    "gold": 1690
}, ignore_index=True)
print( df.loc[[18,19,20],["date","exchange","gold"]])

#修改特定格子的值
df.at[20, "exchange"] = 33  #修改索引值為1的math欄位資料
df.iat[20, 2] = 1700
print( df.loc[[18,19,20],["date","exchange","gold"]])

#針對單一欄位排序
df = df.sort_values(["exchange"], ascending=False)
print(df["exchange"])

#篩選

print(df[ df["exchange"]>32])