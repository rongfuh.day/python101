#使用套件 numpy, matplotlib
#在terminal內，安裝套件 
#用系統管理員，開啟Power Shell
# pip install numpy
# pip install matplotlib

#匯入套件，簡稱為np
import numpy as np
#匯入套件，簡稱為plt
import matplotlib.pyplot as plt

#matplotlib是常用的統計繪圖套件
#它常常使用ndarray格式的資料
#網路參考資源 https://www.w3schools.com/python/matplotlib_pyplot.asp

#繪製散佈圖
xpoints = np.array([0, 1, 2, 3, 4])
ypoints = np.array([0, 3, 2, 1, 7])

plt.plot(xpoints, ypoints)
plt.show()

