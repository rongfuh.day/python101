# 全國有多少條路或街
def Q1():
    print("第一題程式區塊")
    # read csv file to data
    data = []    # store all of the data in csv
    file = open("file_road.csv", "r", encoding = "UTF-8")    # open file
    file.readline()    # skip the first row (title)
    for row in file:
        row = row.replace("\n", "")    # remove the newline character
        data.append(row.split(','))     # split each row by ',' and put them to data
    file.close()    # close file
    # data[i][0]: county
    # data[i][1]: county and town
    # data[i][2]: road

    # find how many roads(路) or streets(街) are there totally
    count = 0
    for i in range(len(data)):
        # check if the LAST character is roads(路) or streets(街)
        if ("路" == data[i][2][-1]) or ("街" == data[i][2][-1]):
            count += 1
    print("全國有", count, "條路或街")

# 南投縣與彰化縣共有多少條路或街
def Q2():
    print("第二題程式區塊")
    # read csv file to data
    data = []    # store all of the data in csv
    file = open("file_road.csv", "r", encoding = "UTF-8")    # open file
    file.readline()    # skip the first row (title)
    for row in file:
        row = row.replace("\n", "")    # remove the newline character
        data.append(row.split(','))     # split each row by ',' and put them to data
    file.close()    # close file
    # data[i][0]: county
    # data[i][1]: county and town
    # data[i][2]: road
    
    # find how many roads(路) or streets(街) are there in 彰化 and 南投
    count = 0
    for i in range(len(data)):
        # check if the LAST character is roads(路) or streets(街)
        if ("彰化縣" in data[i][1]) or ("南投縣" in data[i][1]):    #also "彰化縣" == data[i][0]
            if ("路" == data[i][2][-1]) or ("街" == data[i][2][-1]):
                count += 1
    print("南投縣與彰化縣共有", count, "條路或街")


# 列出路名中包含「中山」或者「臺灣」兩個字的路或街
def Q3():
    print("第三題程式區塊")
    # read csv file to data
    data = []    # store all of the data in csv
    file = open("file_road.csv", "r", encoding = "UTF-8")    # open file
    file.readline()    # skip the first row (title)
    for row in file:
        row = row.replace("\n", "")    # remove the newline character
        data.append(row.split(','))     # split each row by ',' and put them to data
    file.close()    # close file
    # data[i][0]: county
    # data[i][1]: county and town
    # data[i][2]: road
    
    # print all roads(路) or streets(街) which included "中山" or "臺灣" in their names
    for i in range(len(data)):
        if ("路" == data[i][2][-1]) or ("街" == data[i][2][-1]):
            if ("中山" in data[i][2]) or ("臺灣" in data[i][2]):
                print(data[i][1]+data[i][2])

# 請算出路名中包含「龍富」兩個字的路或街的數目
def Q4():
    print("第四題程式區塊")
    # read csv file to data
    data = []    # store all of the data in csv
    file = open("file_road.csv", "r", encoding = "UTF-8")    # open file
    file.readline()    # skip the first row (title)
    for row in file:
        row = row.replace("\n", "")    # remove the newline character
        data.append(row.split(','))     # split each row by ',' and put them to data
    file.close()    # close file
    # data[i][0]: county
    # data[i][1]: county and town
    # data[i][2]: road
    
    # find how many roads(路) or streets(街) included "龍富" in their names
    # and check if the LAST character is roads(路) or streets(街)
    count = 0
    for i in range(len(data)):
        if ("龍富" in data[i][2]) and (("路" == data[i][2][-1]) or ("街" == data[i][2][-1])):
            count += 1
    print("路名中包含「龍富」兩個字的路或街有", count, "個")


# 臺中市與臺北市共有幾個區
def Q5():
    print("第五題程式區塊")
    # read csv file to data
    data = []    # store all of the data in csv
    file = open("file_road.csv", "r", encoding = "UTF-8")    # open file
    file.readline()    # skip the first row (title)
    for row in file:
        row = row.replace("\n", "")    # remove the newline character
        data.append(row.split(','))     # split each row by ',' and put them to data
    file.close()    # close file
    # data[i][0]: county
    # data[i][1]: county and town
    # data[i][2]: road
    
    # find how many districts are there in 臺北市 and 臺中市

    # Method 1: use list
    '''
    dist = []    # the names of the districts in 臺北市 and 臺中市
    for i in range(len(data)):
        if ((data[i][0] == "臺北市") or (data[i][0] == "臺中市")) and ("區" == data[i][1][-1]):
            # if the district of data[i] already in list, then skip
            if data[i][1] in dist:
                continue
            # if the district of data[i] is not in list, then put it in to the list
            else:
                dist.append(data[i][1])
                count += 1
    count = len(dist)
    '''

    # Method 2: use dictionary
    distDict = {}    # the names of the districts in 臺北市 and 臺中市
    for i in range(len(data)):
        # if data[i] is a district and the LAST character is distrcts(區), put it into distDict, 
        # setting the value to "True" means the key has existed
        if ((data[i][0] == "臺北市") or (data[i][0] == "臺中市")) and ("區" == data[i][1][-1]):
            distDict[data[i][1]] = True
    count = len(distDict)
    
    print("臺中市與臺北市共有", count, "個區")

# 全國有幾個「中山區」
def Q6():
    print("第六題程式區塊")
    # read csv file to data
    data = []    # store all of the data in csv
    file = open("file_road.csv", "r", encoding = "UTF-8")    # open file
    file.readline()    # skip the first row (title)
    for row in file:
        row = row.replace("\n", "")    # remove the newline character
        data.append(row.split(','))     # split each row by ',' and put them to data
    file.close()    # close file
    # data[i][0]: county
    # data[i][1]: county and town
    # data[i][2]: road
    
    # find how many 中山區 are there in Taiwan
    # Method 1: use list
    '''
    # dist = []    # the names of the districts
    # count = 0
    # for i in range(len(data)):
    #     if "中山區" in data[i][1] and not (data[i][1] in dist):
    #         dist.append(data[i][1])
    #         count += 1
    '''
    
    # Method 2: use dictionary
    distDict = {}
    for i in range(len(data)):
        # if data[i] is a district, put it into distDict
        # setting the value to "True" means the key has existed
        if "中山區" in data[i][1] and not (data[i][1] in distDict):
            distDict[data[i][1]] = True
    count = len(distDict)
    
    print("全國有", count, "個「中山區」")


# Q1()
# Q2()
# Q3()
# Q4()
Q5()
Q6()
