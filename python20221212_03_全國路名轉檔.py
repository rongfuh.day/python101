#將a檔案轉存為b檔案
def Q1():
       
    #唯讀檔案
    a=open("file_road_null.csv","r",encoding="UTF-8")
    #可寫入檔案
    b=open("file_road_clean.csv","w",encoding="UTF-8")

    #從a讀取第一行表頭
    #並且把a的紀錄指標移到下一筆記錄
    row=a.readline()
    #寫入b檔案中
    b.write(row)

    #針對每一行紀錄進行處理
    for line in a:
        row=a.readline()
        b.write(row)

    #關閉檔案
    b.close()
    a.close()

#將a檔案中南投縣的「路」與「街」的紀錄轉存到「file_road_南投.csv」
def Q2():
    #將a檔案轉存為b檔案
    
    #唯讀檔案
    a=open("file_road_null.csv","r",encoding="UTF-8")
    #可寫入檔案
    b=open("file_road_南投.csv","w",encoding="UTF-8")

    #從a讀取第一行表頭
    #並且把a的紀錄指標移到下一筆記錄
    row=a.readline()
    #寫入b檔案中
    b.write(row)

    #針對每一行紀錄進行處理
    for line in a:        
        #移除字串尾巴的\n新行符號
        row=line.replace("\n", "")    
        #把字串切割成不同欄位
        cols=row.split(',')        
        
        #準備寫入b檔案的紀錄
        new_row=""
        
        #選擇符合條件的紀錄，寫入b檔案中
        if('南投縣' in cols[0]) and cols[2]!="" and (cols[2][-1]=="路" or cols[2][-1]=="街"):            
            new_row=cols[0]+","+cols[1]+","+cols[2]+"\n"
            b.write(new_row)
        
    #關閉檔案
    b.close()
    a.close()

#

#Q1()
Q2()