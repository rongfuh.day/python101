#使用套件 pandas
#用系統管理員，開啟Power Shell
# pip install pandas

# pandas 網路參考資源：https://www.w3schools.com/python/pandas/default.asp

#匯入套件，簡稱為np
import numpy as np
#匯入套件，簡稱為plt
import matplotlib.pyplot as plt
#匯入套件，簡稱為pd
import pandas as pd


#檔案來源 https://data.gov.tw/dataset/8116

# dataframe 網路參考資源 https://www.w3schools.com/python/pandas/pandas_dataframes.asp
# dataframe 網路參考資源 https://www.learncodewithmike.com/2020/11/python-pandas-dataframe-tutorial.html

#載入csv檔，用dataframe格式存放資料


df = pd.read_csv('file_歷年國內主要觀光遊憩據點遊客人數月別統計.csv')
print(df)

#取得前三列資料
print(df.head(3))

#取得後三列資料
print(df.tail(3))


#用欄位名稱取得單一欄位的整欄資料
print( df["1月"])

#at[資料索引值,欄位名稱] 利用資料索引值及欄位名稱來取得「單一值」
print( df.at[1,"1月"])

print( df.iat[1,5])

print( df["1月"].describe())
